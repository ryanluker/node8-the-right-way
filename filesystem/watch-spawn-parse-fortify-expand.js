const fs = require('fs');
const spawn = require('child_process').spawn;
const filename = process.argv[2];
const processToSpawn = process.argv[3];
const processArgs = process.argv.splice(4);

if (!filename) {
    throw Error('A file to watch must be provided!');
}
if (!fs.existsSync(filename)) {
    throw Error('No file with that name!');
}
fs.watch(filename, (event) => {
    if (event === 'rename') {
        console.log('file renamed or removed!');
        return ;
    }
    const ls = spawn(processToSpawn, [...processArgs, filename]);
    let output = '';
    ls.stdout.on('data', chunk => output += chunk);

    ls.on('close', () => {
        const parts = output.split(/\s+/);
        console.log([parts[0], parts[4], parts[8]]);
    });
});
console.log(`Watching file ${filename} for changes!`);
