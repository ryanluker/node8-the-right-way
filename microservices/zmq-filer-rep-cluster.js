'use strict';

const cluster = require('cluster');
const fs = require('fs');
const zmq = require('zeromq');

const numWorkers = require('os').cpus().length;

if (cluster.isMaster) {
    //master process creates router and dealer sockets and binds endpoints
    const router = zmq.socket('router').bind('tcp://127.0.0.1:60401');
    const dealer = zmq.socket('dealer').bind('ipc://filer-dealer.ipc');

    //forward messages between the router and dealer
    router.on('message', (...frames) => dealer.send(frames));
    router.on('message', (...frames) => router.send(frames));

    //listen for workers to come online
    cluster.on(
        'online',
        worker => console.log(`Worker ${worker.process.pid} is online`)
    );

    //fork a worker process for each cpu
    for (let i = 0; i < numWorkers; i++) {
        cluster.fork();
    }
} else {
    //worker processes create a REP socket and connect to the DEALER
    const responder = zmq.socket('rep').connect('ipc://filer-dealer.ipc');

    responder.on('message', data => {
        //parse incoming message
        const request = JSON.parse(data);
        console.log(`${process.pid} received request for: ${request.path}`);

        //read the file and reply with content
        fs.readFile(request.path, (err, content) => {
            console.log(`${process.pid} sending response`);
            responder.send(JSON.stringify({
                content: content.toString(),
                timestamp: Date.now(),
                pid: process.pid
            }));
        });
    });
}